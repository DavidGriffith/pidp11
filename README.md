# PIDP11

A suite of programs for emulating a DEC PDP-11/70 on the Raspberry Pi,
Beaglebone, or Angstrom single board computers mounted in a PiDP11 front
panel.  See https://obsolescence.wixsite.com/obsolescence/pidp-11.

This is based on Stephen Casner's work at
https://github.com/slcasner/pidp11.  Much of the documentation here is
taken from there.  The objective of this respository is to provide a
package that is easily compiled specifically for whatever single-board
computer you choose to put in your PiDP11. Initially, this is just the
Raspberry Pi line.

This package is in a good deal of flux right now.  Eventually you should
be able to clone it, do `make ; sudo make install` and everything will
work.

---

## Prerequisites

### Required
- libreadline-dev	(required for command-line editing in simh)
- screen		(required for maintaining the PDP-11 console)

### Optional
- libsdl2-dev	(optional for PDP-11 graphical terminal emulation)
- libpcap-dev	(optional for PDP-11 networking)

Install these by starting with `sudo apt-get update` and then
`sudo apt-get install libreadline-dev screen`.
Add the other two if you like (highly recommended).

---

## Install Instructions

This assumes you will connect to your Pi remotely.

* Make sure your Pi is connected to the internet.

* Create /opt.  Make sure it's owned by you, not root.

* Go there and pull this repository into it.

* Go into /opt/pidp11 and type `make`.  This builds the compiled
  portions of the emulation.

* Type `cd /etc/init.d ; sudo ln -s /opt/pidp11/etc/rc.pidp11 pidp11`.
  Then type `update-rc.d pidp11 defaults`.  This will make sure the
  simulation starts up when the Pi starts up.

* Go to your home directory and type `ln -s /opt/pidp11/etc/pdp.sh pdp.sh`.
  This creates a means of accessing a terminal on the emulated PDP11.

At this point, you should see some activity on the PiDP11's lights.  If
you don't see anything for 15 seconds, reboot the Pi.  Log back in and
execute `./pdp.sh`.  This will bring up the PiDP-11/70 boot menu.  Only
*idled* is available.  The rest must be installed separately.

